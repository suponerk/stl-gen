// To actually be able to display anything with three.js,
// we need three things: scene, camera and renderer,
// so that we can render the scene with camera

var renderer = new THREE.WebGLRenderer({antialias:true});

// Full screen
var width = window.innerWidth;
var height = window.innerHeight;

// Custom
//var width = 800;
//var height = 500;

renderer.setSize( width, height );

//renderer.setClearColor(0xefefef,1);
//renderer.setClearColor(0x3с3с3с);

document.body.appendChild( renderer.domElement );

var scene = new THREE.Scene();

// XZ Grid Surface creation and adding to scene
//var gridXZ = new THREE.GridHelper(100, 10);
//gridXZ.setColors( new THREE.Color(0xff0000), new THREE.Color(0xffffff) );
//scene.add(gridXZ)


function makeObj() {
	let group = new THREE.Group();	
	
	const geometry = new THREE.BoxGeometry( 100, 100, 100 );
    const material = new THREE.MeshBasicMaterial( { color: 0x0055ee } );
	
	const edges = new THREE.EdgesGeometry( geometry ); // or WireframeGeometry( geometry )
	const lines = new THREE.LineBasicMaterial( { color: 0x999999, linewidth: 1 } );
	
	group.add ( new THREE.Mesh( geometry, material ) );
	group.add ( new THREE.LineSegments( edges, lines ) );
	
    return group;	
}

const object = makeObj();
scene.add( object );

var camera = new THREE.PerspectiveCamera( 45, width / height, 1, 10000 );
camera.position.x = 160;
camera.position.z = 400;
camera.lookAt (new THREE.Vector3(0,0,0));

var controls = new THREE.OrbitControls( camera, renderer.domElement );
//controls.enableDamping = true;
//controls.dampingFactor = 0.25;
//controls.enableZoom = true;
controls.autoRotate = true;

//View 3D model
function animate() {
	controls.update();
	requestAnimationFrame( animate );	
	renderer.render( scene, camera );
	
	//cube.rotation.x += 0.01;
    //cube.rotation.y += 0.01;
}



// Download STL
var exporter = new THREE.STLExporter();
var file = exporter.parse(scene);
function download(data, filename, type) {
	var file = new Blob([data], {type: type});
	if (window.navigator.msSaveOrOpenBlob) // IE10+
		window.navigator.msSaveOrOpenBlob(file, filename);
	else { // Others
		var a = document.createElement("a"),
			url = URL.createObjectURL(file);
		a.href = url;
		a.download = filename;
		document.body.appendChild(a);
		a.click();
		setTimeout(function() {
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);  
		}, 0); 
	}
}




animate();
//download(file, 'Object.stl', 'text/plain');
