# Web Viewer for 3d models using Three.js library

Simple web viewer for 3d models

Key features:
- Using three.js library to render objects
- No build required
- Tool to view 3d models online (now just test cube available for example)

Demo (used gitlab CI):
 <https://suponerk.gitlab.io/stl-gen/>
